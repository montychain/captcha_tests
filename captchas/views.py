import requests
from captcha_tests.settings import *
from django.shortcuts import render
from .forms import NameForm


def index(request):
    if request.method == 'POST':
        form = NameForm(request.POST)
        if form.is_valid():
            your_name = request.POST["your_name"]
            user_IP = request.META['REMOTE_ADDR']
            recaptcha_resp = request.POST["g-recaptcha-response"]
            form = NameForm()
            google_resp = verify_captcha(recaptcha_resp, user_IP)
            data = {
                'form': form,
                'name': your_name,
                'user_IP': user_IP,
                'recaptcha_resp': recaptcha_resp,
                'google_resp': google_resp.text,
            }
            return render(request, 'captchas/index.html', data)
    else:
        form = NameForm()

    return render(request, 'captchas/index.html', {'form': form})


def verify_captcha(recaptcha_resp, user_IP):
    data = {
        "secret": RECAPTCHA_SECRET,
        "response": recaptcha_resp,
        "remoteip": user_IP
    }
    r = requests.post(GOOGLE_VERIFICATION_URL, data=data)
    return r

